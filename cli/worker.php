<?php
declare(ticks = 1); // otherwise signals would be processed only upon pcntl_signal_dispatch(); call
ob_implicit_flush();
$stopTheLoop = false;
// signal handler function
function sig_handler($signo) {
    global $stopTheLoop;
     switch ($signo) {
         case SIGTERM:
         case SIGINT:
             // handle shutdown tasks
             echo "\nShutting down...\n";
             //sleep(2);
             //fwrite($fh, 'end');
             $stopTheLoop = true;
             //exit;
         break;
         default:
             // handle all other signals
     }
}
echo "\nInstalling signal handler...\n";
// setup signal handlers
pcntl_signal(SIGTERM, "sig_handler");
pcntl_signal(SIGINT, "sig_handler");
echo "Done.\n";

if (!empty($argv[1])) chdir($argv[1]);

$meaningfulGitOutputs = array();
$historyLength = 3;
// open file for writing - indicate I am alive
$fh = fopen(__DIR__ . '/../pipes/alive.txt', 'c+');
$canLock = flock($fh, LOCK_EX | LOCK_NB);
if (!$canLock) exit('already running.');
$myPid = getmypid();

while(!$stopTheLoop) {
    ftruncate($fh, 0);
    fwrite($fh, $myPid.'::'.date('Y-m-d H:i:s')."\n");
    
    $exitCode = 0;
    $commandOutput = executeCommand('git pull', $exitCode);
    // TODO: call `git pull` and analyse output
    // if output is meaningful - log it
    $gitOuputIsMeaningful = (
        false !== strpos($commandOutput, 'Updating') ||
        $exitCode != 0
    );
    if ($gitOuputIsMeaningful) {
        if (count($meaningfulGitOutputs) == $historyLength) {
            array_shift($meaningfulGitOutputs);
        }
        $meaningfulGitOutputs[] = array(
            'time' => date('Y-m-d H:i:s'),
            'message' => $commandOutput//'test '.time()
        );
        fwrite($fh, json_encode($meaningfulGitOutputs));
    }
    
    //usleep(500000);
    usleep(5000000);
}
echo "\nDone.\n";

function executeCommand($command, &$exitCode) {
    $exitCode = 0;
    ob_start();
    // look for php stream wrappers
    // TODO: maybe proc_open with $fp = fopen('php://temp/maxmemory:256000', 'w'); for sockets
    passthru($command.' 2>&1', $exitCode);
    $output = ob_get_clean();
    // control stdout, stderr, exitcode separately
    return $output;
}