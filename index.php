<?php
$workerWorkingDir = dirname(__DIR__);
// TODO: check requirements in index.php - e.g. shell_exec, etc. is available
// TODO: push to bitbuacket
// TODO: add uptime
// TODO: better GUI + ajax + constant GUI update, new record effect
// TODO: last active: - <time ago>
// TODO: worker running: Yes/No
// TODO: call 'git pull' and detect meaningful runs, also support svn
// TODO: if worker unstable - create savior script
// TODO: replace __DIR__ with another constant, e.g. TOOL_ROOT
function isRunning() {
    $fh = fopen(__DIR__ . '/pipes/alive.txt', 'r');
    $locked = flock($fh, LOCK_SH | LOCK_NB);
    $workerIsRuning = !$locked;
    if ($locked) {
        flock($fh, LOCK_UN);
    }
    fclose($fh);
    return $workerIsRuning;
}

function getWorkerInfo() {
    $fh = fopen(__DIR__ . '/pipes/alive.txt', 'r');
    list($pid, $lastActive) = explode('::', rtrim(fgets($fh)));
    $pid = trim($pid);
    return array(
        'pid' => $pid,
        'lastActive' => $lastActive
    );
}

function startWorker(array $args=array()) {
    $command = 'php @workerPhpPath @argsList > /dev/null &';//nohup ?
    $preparedCommand = strtr($command, array(
        '@workerPhpPath' => escapeshellarg(__DIR__.'/cli/worker.php'),
        '@argsList' => implode(' ', array_map('escapeshellarg', $args))
    ));
    return shell_exec($preparedCommand);
}

function getMessages() {
    $lines = file(__DIR__ . '/pipes/alive.txt');
    unset($lines[0]);
    $messages = json_decode(implode('', $lines), true);
    return $messages;
}

$workerIsRuning = isRunning();
list($pid, $lastActive) = array_values(getWorkerInfo());

if (!empty($_REQUEST['kill'])) {
    posix_kill($pid, 2/*SIGINT*/);
    while(true) {
        if (!isRunning()) break;
        usleep(500000);
    }//TODO: after some time - kill with SIGKILL (SIGTERM?)
    header('location: ?done');
}
if (!empty($_REQUEST['run'])) {
    // todo: error to separate file ?
    startWorker(array($workerWorkingDir));
    while(true) {
        if (isRunning()) break;
        usleep(500000);
    }
    header('location: ?done');
}

echo '<pre>';
echo 'worker running: ';var_dump($workerIsRuning);
echo "\n";

echo 'pid: '.$pid;
echo "\n";

echo 'last active: '.$lastActive;
echo "\n";
?>
<?php if($workerIsRuning): ?>
<a href="?kill=1">kill</a>
<?php else:?>
<a href="?run=1">run</a>
<?php endif?>

<?php foreach(getMessages() as $message): ?>
<div>
    <?php echo $message['time']?>: <?php echo $message['message']?>
</div>
<?php endforeach; ?>
